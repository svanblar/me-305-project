## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This is the documentation for an automated spice rack completed for ME 305
#
#  @section proj_prop Project Goals
#  The goals of this project were to allow a user to measure the weight of and 
#  eject a spice, all through a phone app. This would be achieved through the 
#  use of a load cell, motor, encoder, and bluetooth antenna.
#  
#  
#  @section taskdia Task Diagram
#  The task diagram is shown below.
#  @image html taskdiagram.png
#  
#  
#
#  @section statedia State Diagrams
#  The state diagrams for each task are shown below.
#  
#  @subsection ui User Interface State Diagram
#
#  @image html UIstate.png
#
#  @subsection mo Motor State Diagram
#
#  @image html motorstate.png
# 
#  @subsection enc Encoder State Diagram
#
#  @image html encodestate.png
#
#  @subsection con Controller State Diagram
#
#  @image html controlstate.png
#
#  @subsection load Load Cell State Diagram
# 
#  @image html loadstate.png
#
#  @section softw Software Development
#
#
#  @author Samara Van Blaricom
#
#  @date December 4, 2020
#