## @file main.py
#  
#
#  
#
#  @author Samara Van Blaricom
#
#  @date November 19, 2020
#
#  
#  @brief 
#

import pyb
from pyb import UART
from pyb import I2C
import ustruct
import utime
import shares
from controller import ProportionController
from encoder import EncoderReader
from motor import MotorDriver
from UserInterface import Task_UI
from loadcelltask import loadcelltask



# device address 0x2A
# Create the pin objects used for interfacing with the motor driver and encoder
pin_EN = pyb.Pin (pyb.Pin.board.PA10, pyb.Pin.OUT_PP)
pin_IN1 = pyb.Pin (pyb.Pin.board.PB4, pyb.Pin.OUT_PP)
pin_IN2 = pyb.Pin (pyb.Pin.board.PB5, pyb.Pin.OUT_PP)
EN1_A = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
EN1_B = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)
tim4 = pyb.Timer(4,prescaler=0,period=0xffff)
EN2_A = None;
EN2_B = None;
tim8 =  None;
    
# Create the timer object used for PWM generation
tim = pyb.Timer(3, freq=20000)
    
# Create a motor object passing in the pins and timer
moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
    
# Set up the encoder and update it
enc = EncoderReader(EN1_A, EN1_B, tim4, EN2_A, EN2_B, tim8)

# Set up the proportional controller
pcon = ProportionController(.13, 0)

    
# Set up load cell
lcell = loadcelltask(1, 0x2A)

# Set up user interface
UI_task = Task_UI()

## The task list contains the tasks to be run "round-robin" style
taskList = [UI_task,
            moe,
            pcon,
            enc,
            lcell]

while True:
    for task in taskList:
        task.run()




    
 
