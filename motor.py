## @file motor.py
#  Brief doc for motor.py
#
#  Detailed doc for motor.py 
#
#  @author Samara Van Blaricom
#
#  @date April 24, 2020
#
#  @package motor
#  A module that runs a small gear motor in the forward or backward direction.
#
#

import shares
import pyb
import utime

'''A motor driver object'''
class MotorDriver:
    
    S0_INIT = 0
    S1_EJECTING = 1
    S2_STOPPEDE = 2
    S3_PLACING = 3
    S4_STOPPEDP = 4
    

    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param EN_pin  A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer   A pyb.Timer object to use for the PWM generation on IN1_pin
                       and IN2_pin. '''
        
        import pyb
        # Declare the variable EN_pin as pin PA10
        self._EN_pin = EN_pin
        # Declare the variable IN1_pin as pin PB4
        self._IN1_pin = IN1_pin
        # Declare the variable IN2_pin as pin PB5
        self._IN2_pin = IN2_pin
        # Create the timer
        self._timer = timer
        # Disable the motor for safety
        self._EN_pin.low ()
        
        self.state = self.S0_INIT

        
        
    def run(self):
        
        if (self.state == self.S0_INIT):
            self.enable()
            self.transitionTo(self.S4_STOPPEDP)
        elif (self.state == self.S1_EJECTING):
            
            self.set_duty(-shares.actusig)
            if (shares.actusig == 0):
                shares.runcmd = None
                self.transitionTo(self.S2_STOPPEDE)
            else:
                self.state == self.S1_EJECTING
        elif(self.state == self.S2_STOPPEDE):
            utime.sleep(10)
            self.set_duty(10)
            self.state = self.S3_PLACING
            
        elif(self.state == self.S3_PLACING):
            self.set_duty(-shares.actusig)
            if (shares.actusig == 0):
                shares.runcmd = None
                self.transitionTo(self.S4_STOPPEDP)
            else:
                self.state == self.S3_PLACING
        elif(self.state == self.S4_STOPPEDP):
            if(shares.runcmd):
                self.set_duty(-10)
                self.state = self.S1_EJECTING
            else:
                self.state = self.S4_STOPPEDP
                
            
                
            
            
            
        
    def enable (self):
        '''Enable the motor'''
        # Enable the motor by turning pin PA10 on
        self._EN_pin.high ()
        
        
    
    def disable (self):
        '''Disable the motor'''
        # Disable the motor by turning pin PA10 off
        self._EN_pin.low ()
        
        
        
    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
            cycle of the PWM signal sent to the motor '''
        import pyb
        # Create an if/then statement based on the value of the duty variable
        if duty <= 0:
            # Set motor to run in a counter-clockwise direction by sending the
            #signal to the IN2_pin
            t3ch2 = self._timer.channel(2, pyb.Timer.PWM, pin=self._IN2_pin)
            t3ch2.pulse_width_percent(0)  
            t3ch1 = self._timer.channel(1, pyb.Timer.PWM, pin=self._IN1_pin)
            t3ch1.pulse_width_percent(abs(duty))
            
        else:
            # Set motor to run in a clockwise direction by sending the
            #signal to the IN1_pin
            t3ch1 = self._timer.channel(1, pyb.Timer.PWM, pin=self._IN1_pin)
            t3ch1.pulse_width_percent(0)
            t3ch2 = self._timer.channel(2, pyb.Timer.PWM, pin=self._IN2_pin)
            t3ch2.pulse_width_percent(duty)
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
            
        
if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__=='__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    import pyb
    
    # Create the pin objects used for interfacing with the motor driver
    pin_EN = pyb.Pin (pyb.Pin.board.PA10, pyb.Pin.OUT_PP)
    pin_IN1 = pyb.Pin (pyb.Pin.board.PB4, pyb.Pin.OUT_PP)
    pin_IN2 = pyb.Pin (pyb.Pin.board.PB5, pyb.Pin.OUT_PP)
    
    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq=20000)
    
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
    
    # Enable the motor driver
    moe.enable()
    
    # Set the duty cycle to 10 percent
    moe.set_duty(10)
 
    
    
    
  
