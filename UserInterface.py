'''
@file UserInterface.py
@brief User interface task as a finite state machine
@author Samara Van Blaricom
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import shares
import utime
from pyb import UART
from BluetoothDriver import Bluetooth

class Task_UI:
    '''
    User interface task.
    
    '''

    ## 0 is eject spice and 1 is measure
    
    ## Initialization state
    S0_INIT             = 0
    
    ## Waiting for character state
    S1_WAIT_FOR_CHAR    = 1
    
    ## Waiting for response state
    S2_WAIT_FOR_RESP    = 2

    def __init__(self):
        '''
        Creates a user interface task object.
        '''
        
        ## The initial state for the User Interface
        self.state = self.S0_INIT
        
        ## Setting up the Bluetooth communication
        self.blu2th = Bluetooth()
        
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        if (self.state == self.S0_INIT):
            ## Run State 0 code
            self.transitionTo(self.S1_WAIT_FOR_CHAR)
            
        elif (self.state == self.S1_WAIT_FOR_CHAR):
            ## Run State 1 code
            if self.blu2th.check():
                if (self.bu2th.readUART() == 0):
                    shares.runcmd = True
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                elif (self.blu2th.readUART() == 1):
                    shares.weighcmd = True
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                else:
                    self.blu2th.writeontoUART("Error: Bluetooth task malfunction")
                    self.transitionTo(self.S0_INIT)
        elif(self.state == self.S2_WAIT_FOR_RESP):
                # Run State 2 Code
                if shares.weight:
                    self.transitionTo(self.S1_WAIT_FOR_CHAR)
                    self.blu2th.writeontoUART(shares.weight)
                    shares.weight = None
                elif shares.eject:
                    self.transitionTo(self.S1_WAIT_FOR_CHAR)
                    shares.eject = None
        else:
            self.blu2th.writeontoUART("Error: Bluetooth task malfunction")
            self.transitionTo(self.S0_INIT)
                    
            
        

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

