## @file controller.py
#  Brief doc for controller.py
#
#  Detailed doc for controller.py 
#
#  @author Samara Van Blaricom
#
#  @date May 13, 2020
#
#  @package controller
#  A module that creates a general proportional controller.
#
#
import shares
import pyb

class ProportionController:
    ''' A proportion controller object '''
    
    S0_INIT = 0
    S1_UPDATING2PLACE = 1
    S2_UPDATING2EJECT = 2
    
    def __init__(self, Kp, Setpoint):
        '''Sets up the proportional controller with the given setpoint and gain.
        @param Kp A user input that defines the proportion of the controller.
        @param Setpoint A user input that defines the input for the controller.'''

        self.Kp = Kp
        self.Setpoint = Setpoint
        
        self.state = self.S0_INIT
        

    def run(self):
        if(self.state == self.S0_INIT):
            if(shares.runcmd):
                self.changeSetpoint(500)
                self.transitionTo(self.S2_UPDATING2EJECT)
            else:
                self.transitionTo(self.S0_INIT)
        elif(self.state == self.S1_UPDATING2PLACE):
            if(shares.delta <= 5):
                self.transitionTo(self.S0_INIT)
                
        elif(self.state == self.S2_UPDATING2EJECT):
            shares.actusig = self.update(shares.delta)
            if (shares.actusig == 0):
                self.changeSetpoint(0)
                self.transitionTo(self.S1_UPDATING2PLACE)
        
        
    def update(self, measuredvalue):
        '''Runs the control algorithm and returns an actuation value
        @param measuredvalue A measurement from the system used to find the error'''
        
        self.measuredvalue = measuredvalue
        self._error = self.Setpoint - self.measuredvalue
        
        self.actuatorsig = self._error*self.Kp
        
        
        return self.actuatorsig
    
    
    def changeSetpoint(self, newSetpoint):
        self.Setpoint = newSetpoint
        
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState