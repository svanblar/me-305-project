'''
@file shares.py
@brief A container for all the inter-task variables
@author Samara Van Blaricom
'''

## The command character sent from the user interface task to the motor task
runcmd     = None

## The command character sent from the user interface task to the load cell task
weighcmd     = None

## The response from the load cell task to the UI after taking a measurement
weight    = None

## The response from the encoder task to the UI after determining the motor  
## has moved to the correct position
eject    = None

## The response from the encoder task to the controller task holding position
delta    = None

## The response from the controller task to the motor task
actusig    = None