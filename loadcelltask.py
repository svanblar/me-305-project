## @file loadcelltask.py
#  
#
#  
#
#  @author Samara Van Blaricom
#
#  @date November 20, 2020
#
#  @package loadcelltask
#
#  @brief A module that employs a state machine to run a load cell
#
import shares
import pyb
from pyb import I2C
from loadcellclass import loadcell_operator

class loadcelltask:
    
    S0_INIT = 0
    S1_READ = 1
    
    def __init__(self, bus, dev_address):
        
        
        self.dev_address = dev_address
        self.bus = bus
        
        self.loadcell = loadcell_operator(self.bus, self.dev_address)
        
        
        
        self.state = self.S0_INIT
        
    def run(self):
        if(self.state==self.S0_INIT):
            if (self.loadcell.checkifready()):
                self.transitionTo(self.S1_READ)
            else:
                shares.weight = "Error"
                self.state = self.S0_INIT
        elif(self.state==self.S1_READ):
            shares.weight = self.loadcell.getreading()
            self.state = self.S0_INIT
        
            
            
        
        
    
    
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState