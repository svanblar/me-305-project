## @file loadcell.py
#  This module allowed us to operate a loadcell.

#  @author Samara Van Blaricom
#
#  @date November 19, 2020
#
#  @package loadcell
#  
#

class loadcell_operator:
    '''A load cell object'''
    
    
    def __init__(self, bus, dev_address):
        '''Creates the loadcell_operator object with the given bus and device address
        @param bus A user input that dictates the bus of the I2C
        @param dev_address The slave device's address'''
        
        ## bus 1
        self.bus = bus
        self.dev_address = dev_address
        self.enable()

                
        
    def enable(self):
        '''Enables and sets up the load cell. '''

        from pyb import I2C
        import ustruct
        
        ## Creates an I2C object used to communicate with the IMU.
        self.i2c = I2C(self.bus, I2C.MASTER)
        
        ## Reset all registers
        self.i2c.mem_write(0b1, self.dev_address, 0x00)  
        
        ## Power up analog and digital sections of scale
        self.i2c.mem_write(0b110, self.dev_address, 0x00)  
        
        ## Set LDO to 3.3V
        currentLDOsetting = self.i2c.mem_read(1, self.dev_address, 0x01)
        c = currentLDOsetting[0] & 0b11000111
        LDO3v3 = c | 0b00100000
        self.i2c.mem_write(LDO3v3, self.dev_address, 0x01)
        
        ## Set gain to 128
        currentgainsetting = self.i2c.mem_read(1, self.dev_address, 0x01)
        g =currentgainsetting[0] & 0b11111000
        gain128 = g | 0b00000111
        self.i2c.mem_write(gain128, self.dev_address, 0x01)
        
        ## Set sample rate to 80 samples per second (SPS)
        currentsamplesetting = self.i2c.mem_read(1, self.dev_address, 0x02)
        s = currentsamplesetting[0] & 0b10001111
        SPS80 = s | 0b00110000
        self.i2c.mem_write(SPS80, self.dev_address, 0x02)
        
        ## Turn off CLK_CHP
        currentclksetting = self.i2c.mem_read(1, self.dev_address, 0x15)
        k = currentclksetting[0] & 0b001111
        clkoff = k | 0b110000
        self.i2c.mem_write(clkoff, self.dev_address, 0x15)
        
        ## Enable 330 pf decoupling cap on chan 2
        currentpwrcntrlsetting = self.i2c.mem_read(1, self.dev_address, 0x1C)
        p = currentpwrcntrlsetting[0] & 0b01111111
        bypasscapenable = p | 0b10000000
        self.i2c.mem_write(bypasscapenable, self.dev_address, 0x1C)
        
        ## Calibrate the load cell 
        self.calibrate()
        
        
        
        
        
    def calibrate(self):
        
        currentcalib = self.i2c.mem_read(1, self.dev_address, 0x02)
        cl = currentcalib[0] & 0b11111011
        startcalib = cl | 0b00000100
        self.i2c.mem_write(startcalib, self.dev_address, 0x02)
        
        
        
    def checkifready(self):
        
        c = self.i2c.mem_read(1, self.dev_address, 0x00)
        
        check = (c[0]>>5) & 0b1
        
        if(check == 0b1):
            return True
        else:
            return False
            
    def getreading(self):
        
        import ustruct
        self.datae = self.i2c.mem_read(3, self.dev_address, 0x12)
        # Unpacks the data into 1 32-bit signed integer
        valuese = ustruct.unpack('<i', self.datae + ('\0' if self.datae[2] < 128 else '\xff'))
        
        return valuese
        
    