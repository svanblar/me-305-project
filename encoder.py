## @file encoder.py
#  This module allowed us to operate an encoder.
#
#  Purpose
#  The purpose of this lab was to allow us to become familiar with encoders and their functionality.
#
#  Usage
#
#  Testing
#  This code was tested in the code itself by inputing a function that gave the class input parameters and function callouts.
#  The code was further tested by physically moving the motor in different directions and using the get_position() command multiple times.
#
#  @author Samara Van Blaricom
#
#  @date May 6, 2020
#
#  @package encoder
#  A module that sets up and reads an encoder.
#
import shares
import pyb
class EncoderReader:
    '''An encoder driver object'''
    
    S0_INIT = 0
    S1_READING = 1
    
    def __init__(self, ENA1_pin, ENB1_pin, timer1, ENA2_pin, ENB2_pin, timer2 ):
        '''Sets up the encoder with given pins and timer
        @param ENA_pin A pyb.Pin object used as the input for the encoder channel A
        @param ENB_pin A pyb.Pin object used as the input for the encoder channel B
        @param timer A pyb.Timer object set to encoder counting mode''' 

        import pyb
        # Declare the ENA_pin variable with the input
        self._ENA1_pin = ENA1_pin
        # Declare the ENB_pin variable with the input
        self._ENB1_pin = ENB1_pin
        # Declare the timer variable with the input
        self._timer1 = timer1
        # Declare the ENA_pin variable with the input
        self._ENA2_pin = ENA2_pin
        # Declare the ENB_pin variable with the input
        self._ENB2_pin = ENB2_pin
        # Declare the timer variable with the input
        self._timer2 = timer2
        
        # Designate the timer channel and mode
        if self._ENA2_pin == None:
            self.tim1ch1 = self._timer1.channel(1,pyb.Timer.ENC_A, pin=self._ENA1_pin)
            self.tim1ch2 = self._timer1.channel(2,pyb.Timer.ENC_B, pin=self._ENB1_pin)
        else:
            self.tim1ch1 = self._timer1.channel(1,pyb.Timer.ENC_A, pin=self._ENA1_pin)
            self.tim1ch2 = self._timer1.channel(2,pyb.Timer.ENC_B, pin=self._ENB1_pin)
            self.tim2ch1 = self._timer2.channel(1,pyb.Timer.ENC_A, pin=self._ENA2_pin)
            self.tim2ch2 = self._timer2.channel(2,pyb.Timer.ENC_B, pin=self._ENB2_pin)
        
        # Set initial values for the actual position
        self.actualposition1 = 0
        self.actualposition2 = 0
            
        self.state = self.S0_INIT
        
    def run(self):
        
        if(self.state == self.S0_INIT):
            if (shares.runcmd):
                self.transitionTo(self.S1_READING)
            else:
                self.transitionTo(self.S0_INIT)
        elif(self.state == self.S1_READING):
            
            if(shares.runcmd):
                shares.delta = self.get_position()
            else:
                self.transitionTo(self.S0_INIT)
                
            
            
        
        
        
    def update(self):
        '''This method allows the user to update the current position of the encoder'''
        # Call for the counter function
        if self._ENA2_pin==None:
            self.position1 = self._timer1.counter()
        else:
            self.position1 = self._timer1.counter()
            self.position2 = self._timer2.counter()
        
        
        
    def get_position(self):
        '''This method allows the user to read the most updated position of the encoder'''
        if self._ENA2_pin==None:
            
            self.get_delta()
            
            self.actualposition1 = self.delta + self.actualposition1
            
            return self.actualposition1
        else:
           
            self.get_delta()

            self.actualposition1 = self.delta1 + self.actualposition1
            self.actualposition2 = self.delta2 + self.actualposition2
            return self.actualposition1
            return self.actualposition2
            
            
    def set_position(self, init_position1, init_position2):
        '''This method allows the user to set the inital position of the encoder
            @param init_position1 A signed integer holding the initial position for encoder 1
            @param init_position2 A signed integer holding the initial position for encoder 2'''
        if init_position2 == None:
            self.position1 = self._timer1.counter(init_position1)
        else:
            self.position1 = self._timer1.counter(init_position1)
            self.position2 = self._timer2.counter(init_position2)
    
    
    def get_delta(self):
        '''This method displays the difference between the two most recent calls to update()'''
        # This if/else statement makes it so that if you only use one encoder you only see one output.
        if self._ENA2_pin==None:
            # Note the initial position
            self.iposition = self.position1
            # Update the position and note the final position
            self.update()
            self.fposition = self.position1
            # Subtract the initial position from the final position
            self.deltai = self.fposition-self.iposition
            # Correct for overflow or underflow depending on the value of deltai
            if self.deltai>=32767.5: 
                self.delta = self.deltai-65535
            elif self.deltai<=-32767.5:
                self.delta = self.deltai + 65535
            else:
                self.delta = self.deltai
            
        # This is a repetition of the code above but for two encoders this time.
        else:
            self.iposition1 = self.position1
            self.iposition2 = self.position2
            self.update()
            self.fposition1 = self.position1
            self.fposition2 = self.position2
            self.deltai1 = self.fposition1-self.iposition1
            self.deltai2 = self.fposition2-self.iposition2
            if self.deltai1>=32767.5:
                self.delta1 = self.deltai1 - 65535
            elif self.deltai1<=-32767.5:
                self.delta1 = self.deltai1 + 65535
            else:
                self.delta1 = self.deltai1
            if self.deltai2>=32767.5:
                self.delta2 = self.deltai2-65535
            elif self.deltai2<=-32767.5:
                self.delta2 = self.deltai2 + 65535
            else:
                self.delta2 = self.deltai2   
                
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
           
            

            
            
        
        




