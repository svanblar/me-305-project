## @file loadcell.py
#  This module allowed us to operate a loadcell.

#  @author Samara Van Blaricom
#
#  @date November 19, 2020
#
#  @package loadcell
#  
#

class loadcell_operator:
    '''A load cell object'''
    
    S0_INIT = 0
    S1_TRIGGERED = 1
    S2_READ = 2
    
    
    def __init__(self, bus, dev_address):
        '''Creates the loadcell_operator object with the given bus and device address
        @param bus A user input that dictates the bus of the I2C
        @param dev_address The slave device's address'''
        
        ## bus 1
        self.bus = bus
        self.dev_address = dev_address
        self.enable()
        self.state = self.S0_INIT
        
    def run(self):
        
        if (self.state == self.S0_INIT):
            self.checkifready()
            self.transitionTo(self.S1_TRIGGERED)
        
        elif (self.state == self.S1_TRIGGERED):
                

        
    def enable(self):
        '''Enables the load cell. '''

        from pyb import I2C
        # Creates an I2C object used to communicate with the IMU.
        self.i2c = I2C(self.bus, I2C.MASTER)
        
        self.i2c.mem_write(0b111, self.dev_address, 0x00)
        
    def checkifready(self):
        
        c = self.i2c.mem_read(1, self.dev_address, 0x00)
        
        check = (c[0]>>3) & 0b1000
        
        if(check == 0b1000):
            print("Load Cell is ready")
        else:
            print("Load Cell not set up correctly")
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
        
    